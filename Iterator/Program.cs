﻿using System;

namespace Iterator
{
    class Programc
    {
        static void Main(string[] args)
        {
            var a = new ConcreteAggregate();
            a[0] = "Item A";
            a[1] = "Item B";
            a[2] = "Item C";
            a[3] = "Item D";
            Iterator iterator = a.CreateIterator();
            Console.WriteLine("Iteragindo com a coleção: ");
            object item = iterator.First();
            int x = 0;
            while (item != null)
            {
                Console.WriteLine(item);
                item = iterator.Next();
                if (x == 2)
                {
                    item = iterator.CurrentItem();
                }
                x++;
            }
            Console.ReadKey();
        }
    }
}
