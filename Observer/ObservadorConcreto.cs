﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    public class ObservadorConcreto : Observador
    {
        private string _nome;
        private string _estadoobservador;
        private AssuntoConcreto _assunto;
        public AssuntoConcreto Assunto { get; set; }

        public ObservadorConcreto(AssuntoConcreto assunto, string nome)
        {
            _assunto = assunto;
            _nome = nome;
        }

        public override void Update()
        {
            _estadoobservador = this._assunto.EstadoAssunto;
            Console.WriteLine("Observador {0}, seu novo estado é: {1} ", _nome, _estadoobservador);
        }
    }
}
