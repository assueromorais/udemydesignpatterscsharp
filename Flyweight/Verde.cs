﻿using System;

namespace Flyweight
{
    public class Verde : Tartaruga
    {
        public Verde()
        {
            condicao = "tartaruga dentro do casco, ";
            acao = "rodando no chão ";
        }
        public override void Mostrar(string qualcor)
        {
            this.cor = qualcor;
            Console.WriteLine(condicao + acao + cor.ToUpper());
        }
    }
}
