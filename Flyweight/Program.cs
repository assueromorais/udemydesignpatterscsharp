﻿using System;

namespace Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {

            var fabrica = new FabricaFlyweight();
            Tartaruga tartaruga;
            string cor = null;

            while (true)
            {
                Console.WriteLine();
                Console.Write("Qual tartaruga enviar para tela: ");
                cor = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(cor))
                    break;
                tartaruga = fabrica.GetTartaruga(cor);
                tartaruga.Mostrar(cor);
                Console.WriteLine();
                Console.WriteLine("------------------");
            }
        }

    }
}
