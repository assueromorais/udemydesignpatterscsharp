﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    public class FabricaFlyweight
    {
        private Dictionary<string, Tartaruga> listaDeTartarugas = new Dictionary<string, Tartaruga>();

        public Tartaruga GetTartaruga(string cor)
        {
            Tartaruga T = null;
            if (listaDeTartarugas.ContainsKey(cor))
            {
                T = listaDeTartarugas[cor];
            }
            else
            {
                switch (cor)
                {
                    case "azul": T = new Azul(); break;
                    case "verde": T = new Verde(); break;
                    case "vermelha": T = new Vermelha(); break;
                    case "laranja": T = new Laranja(); break;
                    default: new Vermelha(); break;
                }
                listaDeTartarugas.Add(cor, T);
            }
            return T;
        }
    }
}
