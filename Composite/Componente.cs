﻿using System;

namespace Composite
{
    public abstract class Componente
    {
        protected string nome;

        public Componente(string pNome)
        {
            nome = pNome;
        }

        public abstract void Adicionar(Componente c);
        public abstract void Remover(Componente c);
        public abstract void Mostrar(int profundidade);

    }
}
