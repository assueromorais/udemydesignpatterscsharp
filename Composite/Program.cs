using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            var root = new Composite("ROOT");
            root.Adicionar(new Folha("Folha A"));
            root.Adicionar(new Folha("Folha B"));

            var comp = new Composite("Composite X");
            comp.Adicionar(new Folha("Folha XA"));
            comp.Adicionar(new Folha("Folha XB"));

            root.Adicionar(comp);
            root.Adicionar(new Folha("Folha C"));
            root.Mostrar(2);
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
