﻿using System;

namespace DependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            var enda = new EnderecoA();
            var endb = new EnderecoB();
            enda.Rua = "Rua Endereço A";
            enda.Numero = 1;
            endb.Cidade = "Blumenau";
            endb.UF = "SC";
            
            var empresa = new Empresa();
            var empresa2 = new Empresa();
        }
    }
}
