﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjection
{
    public class Empresa
    {
        public int Codigo { get; set; }
        public string RazaoSocial { get; set; }
        public IObjetoEndereco Endereco { get; set; }
        public Empresa(IObjetoEndereco objetoEndereco)
        {
            this.Endereco = objetoEndereco;
        }
        public Empresa()
        {
            this.Endereco = Localizador.GetEndereco();
        }
    }
}
