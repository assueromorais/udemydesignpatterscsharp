﻿using System;

namespace ChainOfResponsability
{
    public abstract class Handler
    {
        protected Handler Sucessor;
        public void SetSucessor(Handler sucessor)
        {
            Sucessor = sucessor;
        }

        public abstract void HandleRequest(int request);

    }
}
