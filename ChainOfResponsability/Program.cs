﻿using System;

namespace ChainOfResponsability
{
    class Program
    {
        static void Main(string[] args)
        {
            var h1 = new ConcreteHandler1();
            var h2 = new ConcreteHandler2();
            var h3 = new ConcreteHandler3();
            h1.SetSucessor(h2);
            h2.SetSucessor(h3);
            int[] requests = { 2,5,24,22,18,3,27,20};
            foreach(int request in requests)
            {
                h1.HandleRequest(request);
            }
            Console.ReadKey();
        }
    }
}
