﻿using System;

namespace Adapter
{
    public class RoboInimigo
    {
        Random gerador = new Random();

        public void Esmagar() {
            int danoAtaque = gerador.Next(10) + 1;
            Console.WriteLine("O robo inimigo causou " + danoAtaque + " de dano com o ataque de esmagar com as mãos.");
        } 

        public void AndarParaFrente() {
            int movimento = gerador.Next(10) + 1;
            Console.WriteLine("O robo inimigo andou " + movimento + " passos para frente!");
        }

        public void ReagirContraHumano(string piloto)
        {
            Console.WriteLine("O robo inimigo vai contra " + piloto);
        }

    }
}
