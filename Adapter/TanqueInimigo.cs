﻿using System;

namespace Adapter
{
    public class TanqueInimigo : IAtaqueInimigo
    {
        Random gerador = new Random();
        public void ArmaFogo()
        {
            int danoFogo = gerador.Next(10) + 1;
            Console.WriteLine("Tanque inimigo fez " + danoFogo + " de dano!");
        }

        public void Movimenta()
        {
            int movimento = gerador.Next(5) + 1;
            Console.WriteLine("Tanque inimigo andou " + movimento + " espaços!");
        }

        public void Pilotar(string piloto)
        {
            Console.WriteLine("Piloto " + piloto + " está no comando do tanque agora!");
        }
    }
}
