﻿namespace Adapter
{
    public class RoboInimigoAdapter : IAtaqueInimigo
    {
        RoboInimigo robo;

        public RoboInimigoAdapter(RoboInimigo roboInimigoAdaptado)
        {
            robo = roboInimigoAdaptado;
        }

        public void ArmaFogo()
        {
            robo.Esmagar();
        }

        public void Movimenta()
        {
            robo.AndarParaFrente();
        }

        public void Pilotar(string piloto)
        {
            robo.ReagirContraHumano(piloto);
        }
    }
}
