using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            var rx2018 = new TanqueInimigo();
            var R2D2 = new RoboInimigo();
            IAtaqueInimigo robo_adapter = new RoboInimigoAdapter(R2D2);

            Console.WriteLine(" ========== ROBO ========== ");
            R2D2.ReagirContraHumano("Assuéro");
            R2D2.AndarParaFrente();
            R2D2.Esmagar();

            Console.WriteLine(" ========== ROBO ADAPTADO ========== ");
            robo_adapter.Pilotar("Assuéro");
            robo_adapter.Movimenta();
            robo_adapter.ArmaFogo();

            Console.WriteLine(" ========== TANQUE ========== ");
            rx2018.Pilotar("João");
            rx2018.Movimenta();
            rx2018.ArmaFogo();

            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
