﻿using System;
namespace Mediator
{
    public abstract class Colega
    {
        protected Mediador Mediador;
        public Colega(Mediador mediador)
        {
            Mediador = mediador;
        }
    }
}