﻿using System;

namespace Mediator
{
    public class MediadorConcreto : Mediador
    {
        public ColegaConcreto1 Colega1 { set; protected get; }

        public ColegaConcreto2 Colega2 { set; protected get; }

        public override void Enviar(string mensagem, Colega colega)
        {
            if (colega == Colega1)
                Colega2.Notificar(mensagem);
            else
                Colega1.Notificar(mensagem);
        }

    }
}
