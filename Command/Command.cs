﻿namespace Command
{
    public abstract class Command
    {
        protected Receiver receiver;

        public Command(Receiver rc)
        {
            receiver = rc;
        }

        public abstract void Execute();
    }
}
