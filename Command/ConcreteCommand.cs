﻿
namespace Command
{
    public class ConcreteCommand : Command
    {
        public ConcreteCommand(Receiver rc) : base(rc)
        {
            receiver = rc;
        }
        public override void Execute()
        {
            receiver.Action();
        }
    }
}
