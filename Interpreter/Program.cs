﻿using System;
using System.Collections.Generic;

namespace Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            var contexto = new Contexto("MCMXXVIII");
            var listas = new List<Expressao>();
            listas.Add(new MilharesExpressao());
            listas.Add(new CentenasExpressao());
            listas.Add(new DezenasExpressao());
            listas.Add(new UnidadeExpressao());
            foreach(Expressao expressao in listas)
            {
                expressao.Interpretador(contexto);
            }
            Console.WriteLine("{0} = {1}", "MVMXXVIII", contexto.Output);
            Console.ReadKey();
        }
    }
}
