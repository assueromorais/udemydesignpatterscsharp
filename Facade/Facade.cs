﻿using System;

namespace Facade
{
    public class Facade
    {
        private readonly SubSistemaUm um = null;
        private readonly SubSistemaDois dois = null;
        private readonly SubSistemaTres tres = null;

        public Facade()
        {
            um = new SubSistemaUm();
            dois = new SubSistemaDois();
            tres = new SubSistemaTres();
        }

        public void MetodoA()
        {
            Console.WriteLine("\nMétodoA() ------ ");
            dois.MetodoDois();
            tres.MetodoTres();
        }

        public void MetodoB()
        {
            Console.WriteLine("\nMétodoB() ------ ");
            um.MetodoUm();
            tres.MetodoTres();
        }

    }
}
