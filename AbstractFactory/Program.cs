﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro carro1 = ExecutaAbstractFactory.MontarCarro("luxo");
            Carro carro2 = ExecutaAbstractFactory.MontarCarro("popular");
            Console.ReadKey();
        }
    }
}
