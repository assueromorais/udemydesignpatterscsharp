﻿using System;

namespace AbstractFactory
{
    public class ExecutaAbstractFactory
    {

        public static Carro MontarCarro(string tipo)
        {
            CarroFactory cf = null;
            switch (tipo)
            {
                case "luxo":
                    cf = new CarroLuxoFactory();
                    break;
                case "popular":
                    cf = new CarroPopularFactory();
                    break;
            }
            Carro carro = new Carro();
            carro.roda = cf.MontarRoda();
            carro.som = cf.MontarSom();
            return carro;
        }
    }
}
