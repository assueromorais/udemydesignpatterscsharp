﻿
using System;

namespace AbstractFactory
{
    public class CarroLuxoFactory : CarroFactory
    {
        public override Som MontarSom()
        {
            return new BluetoothPlayer();
        }
        public override Roda MontarRoda()
        {
            return new RodaLigaLeve();
        }
    }
}
