﻿using System;

namespace AbstractFactory
{
    public abstract class CarroFactory
    {
        public abstract Som MontarSom();
        public abstract Roda MontarRoda();
    }
}
