﻿using System;

namespace Singleton
{
    class BolaSemSingleton
    {
        public BolaSemSingleton() {
            Console.WriteLine("Bola em juto.");
        }

        public void Mensagem(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
