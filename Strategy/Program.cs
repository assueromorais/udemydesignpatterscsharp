﻿using System;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = new SortedList();
            s.Add("Luan");
            s.Add("Leônidas");
            s.Add("Eron");
            s.Add("Rueda");
            s.Add("Cestari");
            s.Add("Assuéro");
            s.Add("André");
            s.Add("Acácio");
            s.Add("Eduardo");
            s.Add("Ives");

            s.SetSortStrategy(new QuickSort());
            s.Sort();

            s.SetSortStrategy(new MergeSort());
            s.Sort();

            s.SetSortStrategy(new ShellSort());
            s.Sort();
            Console.ReadKey();
        }
    }
}
