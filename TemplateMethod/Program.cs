﻿using System;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {

            AbstractClass abstrata;
            abstrata = new ConcreteClassA();
            abstrata.TemplateMethod();

            abstrata = new ConcreteClassB();
            abstrata.TemplateMethod();

            Console.ReadKey();
        }
    }
}
