﻿
namespace Builder
{
    public class IPhoneBuilder : ICelular
    {
        public Celular celular { get; }

        public IPhoneBuilder()
        {
            celular = new Celular("IPhone S9");
        }

        public void BuildBateria()
        {
            celular.Bateria = "MAH_1500";
        }

        public void BuildCamera()
        {
            celular.Camera = "16 MP";
        }

        public void BuildSistema()
        {
            celular.SistemaOperacional = "IOS 11";
        }

        public void BuildTela()
        {
            celular.Tela = "9";
        }
    }
}
