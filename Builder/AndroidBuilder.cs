﻿namespace Builder
{
    public class AndroidBuilder : ICelular
    {
        public Celular celular { get; }

        public AndroidBuilder()
        {
            celular = new Celular("Android");
        }

        public void BuildBateria()
        {
            celular.Bateria = "MAH_1500";
        }

        public void BuildCamera()
        {
            celular.Camera = "16 MP";
        }

        public void BuildSistema()
        {
            celular.SistemaOperacional = "Android 8.0";
        }

        public void BuildTela()
        {
            celular.Tela = "7";
        }
    }
}
