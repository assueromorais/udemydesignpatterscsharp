﻿using System;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            var fabricante = new Fabricante();
            ICelular celularBuilder;
            celularBuilder = new AndroidBuilder();
            fabricante.Construtor(celularBuilder);

            Console.WriteLine(string.Format("Um novo Celular foi contruido: {0}", celularBuilder.celular.Nome));
            celularBuilder = new IPhoneBuilder();
            fabricante.Construtor(celularBuilder);
            Console.WriteLine(string.Format("Um novo Celular foi contruido: {0}", celularBuilder.celular.Nome));
            Console.ReadKey();
        }
    }
}
