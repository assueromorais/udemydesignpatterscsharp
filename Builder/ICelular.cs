﻿namespace Builder
{
    public interface ICelular
    {
        void BuildTela();
        void BuildBateria();
        void BuildSistema();
        void BuildCamera();
        Celular celular { get; }
    }
}
