﻿using System;

namespace Memento
{
    class Program
    {
        static void Main(string[] args)
        {
            var o = new Originator();
            o.State = "On";
            var c = new CareTaker();
            c.Memento = o.CreateMemento();
            o.State = "Off";
           o.SetMemento(c.Memento);
            Console.ReadKey();
        }
    }
}
