﻿using System;

namespace Decorator
{
    public class Video : ItemBiblioteca
    {
        public string Diretor { get; set; }
        public string Titulo { get; set; }
        public int MinutosDuracao { get; set; }
        public Video(string diretor, string titulo, int minutosDuracao, int numeroCopias)
        {
            Diretor = diretor;
            Titulo = titulo;
            NumeroCopias = numeroCopias;
            MinutosDuracao = minutosDuracao;
        }
        public override void Exibe()
        {
            Console.WriteLine("\nVídeo ----");
            Console.Write(" Diretor: " + Diretor);
            Console.Write(" Título: " + Titulo);
            Console.Write(" Duração: " + MinutosDuracao);
            Console.Write(" # Cópias: " + NumeroCopias);
        }
    }
}
