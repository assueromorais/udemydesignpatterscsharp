﻿using System;
using System.Collections.Generic;

namespace Decorator
{
    public class Emprestado : Decorator
    {
        protected List<string> Emprestados = new List<string>();
        public Emprestado(ItemBiblioteca itemBiblioteca) : base(itemBiblioteca)
        {

        }

        public void EmprestarItem(string nome)
        {
            Emprestados.Add(nome);
            ItemBiblioteca.NumeroCopias--;
        }

        public void DevolverItem(string nome)
        {
            Emprestados.Remove(nome);
            ItemBiblioteca.NumeroCopias++;
        }

        public override void Exibe()
        {
            base.Exibe();
            foreach(string item in Emprestados)
            {
                Console.WriteLine("Emprestado: " + item);
            }
            Console.WriteLine();
        }

    }
}
