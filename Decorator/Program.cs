﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            //Criar um Livro
            var livro = new Livro("João", "Design Patterns",1);
            livro.Exibe();

            //Criar um Vídeo
            var video = new Video("Rodrigo", "Vídeo Aulas Design Patterns", 120, 1);
            video.Exibe();

            Console.WriteLine("\nEmprestado um Vídeo:");

            Emprestado emprestado = new Emprestado(video);
            emprestado.EmprestarItem("Carlos");
            emprestado.EmprestarItem("Maria");

            emprestado.Exibe();

            emprestado.DevolverItem("Carlos");

            Console.ReadKey();
        }
    }
}
