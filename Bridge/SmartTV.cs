﻿using System;

namespace Bridge
{
    public class SmartTV
    {
        public ICanal canalAtual { get; set; }

        public void ExibeCanalSintonizado()
        {
            if (canalAtual != null)
                Console.WriteLine(canalAtual.Canal());
        }

        public void PlayTv()
        {
            if (canalAtual != null)
                Console.WriteLine(canalAtual.Status());
            else
                Console.WriteLine("Por favor, selecione um canal para assistir algo!");
        }

    }
}
