using System;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            var minhaTv = new SmartTV();
            Console.WriteLine("SELECIONE UM CANAL");
            Console.WriteLine("1  - Filmes. \n 2 - Documentários \n 3 - Culinária");
            ConsoleKeyInfo input  = Console.ReadKey();
            switch (input.KeyChar)
            {
                case '1':
                    minhaTv.canalAtual = new Filme();
                    break;
                case '2':
                    minhaTv.canalAtual = new Documentario();
                    break;
                case '3':
                    minhaTv.canalAtual = new Culinaria();
                    break;
            }
            Console.WriteLine();
            minhaTv.ExibeCanalSintonizado();
            minhaTv.PlayTv();
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
