﻿using System;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            var proxy = new Proxy();
            proxy.Requisicao();
            Console.ReadKey();
        }

    }
}
