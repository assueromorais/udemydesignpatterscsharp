﻿using System;

namespace Visitor
{
    class ConcreteVisitor2 : Visitor
    {
        public override void VisitConcreteElementA(ConcreteElementA concreteElementA)
        {
            Console.WriteLine("{0} visitado por {1}", this.GetType().Name, concreteElementA.GetType().Name);
        }

        public override void VisitConcreteElementB(ConcreteElementB concreteElementB)
        {
            Console.WriteLine("{0} visitado por {1}", this.GetType().Name, concreteElementB.GetType().Name);

        }
    }
}
